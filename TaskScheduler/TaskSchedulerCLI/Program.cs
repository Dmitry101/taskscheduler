﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaskScheduler;

namespace TaskSchedulerCLI
{
    class Program
    {
        static void Main(string[] args)
        {
            FactoriesContainer.Instance.Register<ITaskContainerFactory, TaskContainerFactory>();
            FactoriesContainer.Instance.Register<ITaskFactory, TaskFactory>();
            ITaskRunner taskRunner = new TaskRunner();
            ITaskContainer taskContainer = taskRunner.GetTaskContainer();
            Enumerable.Range(0, 5).ToList()
                .ForEach(i => taskContainer.AddTask("RunNotepad" + i, (i % 2 == 0 ? 2 : 0), "notepad.exe"));
            taskRunner.Start();
            Console.ReadLine();
            taskRunner.Stop();
        }
    }
}
