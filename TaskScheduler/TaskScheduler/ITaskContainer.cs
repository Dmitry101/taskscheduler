﻿using System.Collections.Generic;

namespace TaskScheduler
{
    public interface ITaskContainer
    {
        void AddTask(string name, string processName);
        void AddTask(string name, string processName, string processArguments);
        void AddTask(string name, int period, string processName);
        void AddTask(string name, int period, string processName, string processArguments);
        IEnumerable<ITask> GetTasks();
        ITask GetTaskByName(string name);
        bool Remove(string name);
    }
}
