﻿using System;

namespace TaskScheduler
{
    /// <summary>
    /// The interface determines tasks should be executed after the application sterts.
    /// </summary>
    public interface ITask
    {
        /// <summary>
        /// The required unique name of a task.
        /// </summary>
        string Name { get; set; }
        /// <summary>
        /// The period in seconds with which a task will be repeated.
        /// </summary>
        int Period { get; set; }
        /// <summary>
        /// The date and time when a task is created.
        /// </summary>
        DateTime CreationDate { get; set; }
        /// <summary>
        /// The date and time when a task is executed last time.
        /// </summary>
        DateTime LastRunDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        string ProcessName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        string ProcessArguments { get; set; }
        /// <summary>
        /// Check whether of not the task can be run.
        /// </summary>
        /// <returns>Return true if the task's period is elapsed and taks can be executed again or if the taks is never run before. Othervise - false.</returns>
        bool CheckRun();
        /// <summary>
        /// The method executed a task.
        /// </summary>
        void Run();
    }
}
