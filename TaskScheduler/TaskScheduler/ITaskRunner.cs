﻿using System;

namespace TaskScheduler
{
    public interface ITaskRunner
    {
        ITaskContainer GetTaskContainer();
        void Start();
        void Stop();
    }
}
