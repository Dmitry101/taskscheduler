﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace TaskScheduler
{
    public class TaskRunner : ITaskRunner, IDisposable
    {
        private ITaskContainer taskContainer;
        private List<Timer> timers;
        private bool disposed;

        public TaskRunner()
        {
            ITaskContainerFactory taskContainerFactory = FactoriesContainer.Instance.Resolve<ITaskContainerFactory>();
            taskContainer = taskContainerFactory.GetTaskContainer();
            timers = new List<Timer>();
        }

        public ITaskContainer GetTaskContainer()
        {
            return taskContainer;
        }

        public void Start()
        {
            IEnumerable<ITask> tasks = taskContainer.GetTasks();
            if (tasks != null && tasks.Count() > 0)
            {
                foreach (ITask task in tasks)
                {
                    ScheduleTask(task);
                }
            }
        }

        private void ScheduleTask(ITask task)
        {
            int dueTime = task.Period * 1000;
            int period = task.Period == 0 ? Timeout.Infinite : dueTime;
            Timer timer = new Timer((s) => task.Run(), null, dueTime, period);
            timers.Add(timer);
        }

        public void Stop()
        {
            Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                timers.ForEach(t => t.Dispose());
                timers.Clear();
            }

            disposed = true;
        }

        ~TaskRunner()
        {
            Dispose(false);
        }
    }
}
