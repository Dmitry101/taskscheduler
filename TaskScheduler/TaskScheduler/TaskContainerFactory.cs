﻿using System;

namespace TaskScheduler
{
    public class TaskContainerFactory : ITaskContainerFactory
    {
        public ITaskContainer GetTaskContainer()
        {
            return new TaskContainer();
        }
    }
}
