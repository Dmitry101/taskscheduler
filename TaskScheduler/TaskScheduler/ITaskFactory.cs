﻿using System;

namespace TaskScheduler
{
    public interface ITaskFactory
    {
        ITask CreateTask(string name, string processName);
        ITask CreateTask(string name, string processName, string processArguments);
        ITask CreateTask(string name, int period, string processName);
        ITask CreateTask(string name, int period, string processName, string processArguments);
    }
}
