﻿using System;

namespace TaskScheduler
{
    public interface ITaskContainerFactory
    {
        ITaskContainer GetTaskContainer();
    }
}
