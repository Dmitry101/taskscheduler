﻿using System;

namespace TaskScheduler
{
    public class TaskFactory : ITaskFactory
    {
        public ITask CreateTask(string name, string processName)
        {
            return new Task(name, processName);
        }

        public ITask CreateTask(string name, int period, string processName)
        {
            return new Task(name, period, processName);
        }

        public ITask CreateTask(string name, string processName, string processArguments)
        {
            return new Task(name, processName, processArguments);
        }

        public ITask CreateTask(string name, int period, string processName, string processArguments)
        {
            return new Task(name, period, processName, processArguments);
        }
    }
}
