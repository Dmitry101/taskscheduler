﻿using System.Collections.Generic;
using System.Linq;

namespace TaskScheduler
{
    public class TaskContainer : ITaskContainer
    {
        private ITaskFactory taskFactory;
        private List<ITask> tasks;

        public TaskContainer()
        {
            taskFactory = FactoriesContainer.Instance.Resolve<ITaskFactory>();
            tasks = new List<ITask>();
        }

        public void AddTask(string name, string processName)
        {
            tasks.Add(taskFactory.CreateTask(name, processName));
        }

        public void AddTask(string name, int period, string processName)
        {
            tasks.Add(taskFactory.CreateTask(name, period, processName));
        }

        public void AddTask(string name, string processName, string processArguments)
        {
            tasks.Add(taskFactory.CreateTask(name, processName, processArguments));
        }

        public void AddTask(string name, int period, string processName, string processArguments)
        {
            tasks.Add(taskFactory.CreateTask(name, period, processName, processArguments));
        }

        public ITask GetTaskByName(string name)
        {
            return tasks.FirstOrDefault(t => t.Name == name);
        }

        public IEnumerable<ITask> GetTasks()
        {
            return tasks;
        }

        public bool Remove(string name)
        {
            ITask task = GetTaskByName(name);
            if (task != null)
            {
                return tasks.Remove(task);
            }

            return false;
        }
    }
}
