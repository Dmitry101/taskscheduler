﻿using System;
using System.Collections.Generic;

namespace TaskScheduler
{
    public class FactoriesContainer
    {
        private static FactoriesContainer container;
        private Dictionary<Type, Type> factories;

        protected FactoriesContainer()
        {
            factories = new Dictionary<Type, Type>();
        }

        public static FactoriesContainer Instance
        {
            get
            {
                if (container == null)
                {
                    container = new FactoriesContainer();
                }

                return container;
            }
        }

        public void Register<TSource, TTarget>()
            where TTarget : class, TSource, new()
        {
            factories[typeof(TSource)] = typeof(TTarget);
        }

        public TSource Resolve<TSource>()
        {
            if (factories.ContainsKey(typeof(TSource)))
            {
                return (TSource)Activator.CreateInstance(factories[typeof(TSource)]);
            }

            return default(TSource);
        }
    }
}
