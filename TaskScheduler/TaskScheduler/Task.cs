﻿using System;
using System.Diagnostics;
using System.Threading;

namespace TaskScheduler
{
    public class Task : ITask
    {
        private readonly object locker = new object();

        public Task(string name, string processName) : this(name, 0, processName, "") { }

        public Task(string name, string processName, string processArguments) : this(name, 0, processName, processArguments) { }

        public Task(string name, int period, string processName) : this(name, period, processName, "") { }

        public string Name { get; set; }
        public int Period { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastRunDate { get; set; }
        public string ProcessName { get; set; }
        public string ProcessArguments { get; set; }

        public Task(string name, int period, string processName, string processArguments)
        {
            if (name == null)
            {
                name = string.Empty;
            }

            if (string.IsNullOrEmpty(processName))
            {
                throw new ArgumentNullException(nameof(processName));
            }

            Name = name;
            Period = period;
            CreationDate = DateTime.Now;
            LastRunDate = DateTime.MinValue;
            ProcessName = processName;
            ProcessArguments = ProcessArguments;
        }

        public bool CheckRun()
        {
            bool isRepeateble = Period > 0;
            bool neverRun = LastRunDate < CreationDate;
            if (!isRepeateble && neverRun)
                return true;

            bool periodElapsed = LastRunDate.AddMilliseconds(Period) <= DateTime.Now;
            if (isRepeateble && (periodElapsed || neverRun))
                return true;

            return false;
        }

        public void Run()
        {
            if (Monitor.TryEnter(locker))
            {
                try
                {
                    Process.Start(ProcessName, ProcessArguments);
                }
                catch (Exception e)
                {
                    // TODO: Log exception
                }
                finally
                {
                    Monitor.Exit(locker);
                    Console.WriteLine(">> {0}", Name);
                    LastRunDate = DateTime.Now;
                }
            }
        }
    }
}
